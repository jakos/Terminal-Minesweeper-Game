unit cell;
interface
type
    CellTypes = (
                 CHidden, 
                 CEmpty, 
                 CNumber, 
                 CFlag, 
                 CBomb, 
                 CMarkedBomb, 
                 CBorder, 
                 CCursor
                );

    TCell = record
        symbol: char;
        fgcolor, bgcolor: word;
        IsBlink: boolean;
        CellType: CellTypes;
    end;

procedure DrawCell(var CellObj: TCell; x, y: integer);
procedure SetHiddenCell(var CellObj: TCell);
procedure SetEmptyCell(var CellObj: TCell);
procedure SetNumberCell(var CellObj: TCell; number: integer);
procedure SetFlagCell(var CellObj: TCell);
procedure SetMarkedBombCell(var CellObj: TCell);
procedure SetBombCell(var CellObj: TCell);
procedure SetBorderCell(var CellObj: TCell);
procedure SetCursorCell(var CellObj: TCell);

implementation
uses crt;
const
    HiddenCellFgcolor = Green;
    HiddenCellBgcolor = Green;
    HiddenCellSymbol = ' ';
    HiddenCellIsBlink = false;

    NumberCellFgcolor = Blue;
    NumberCellBgcolor = Brown;
    NumberCellIsBlink = false;

    EmptyCellFgcolor = Green;
    EmptyCellBgcolor = Brown;
    EmptyCellSymbol = ' ';
    EmptyCellIsBlink = false;

    FlagCellFgcolor = Red;
    FlagCellBgcolor = Green;
    FlagCellSymbol = 'F';
    FlagCellIsBlink = false;

    BombCellFgcolor = Red;
    BombCellBgcolor = Green;
    BombCellSymbol = '*';
    BombCellIsBlink = false;

    MarkedBombCellFgcolor = White;
    MarkedBombCellBgcolor = Brown;
    MarkedBombCellSymbol = '*';
    MarkedBombCellIsBlink = false;

    BorderCellFgcolor = Black;
    BorderCellBgcolor = LightGreen;
    BorderCellSymbol = '#';
    BorderCellIsBlink = false;

    CursorCellBgcolor = White;
    CursorCellIsBlink = true;


procedure DrawCell(var CellObj: TCell; x, y: integer);
var
    SaveTextAttr: integer;
begin
    SaveTextAttr := TextAttr;
    GotoXY(x, y);
    if CellObj.IsBlink then
        TextColor(CellObj.fgcolor + blink)
    else
        TextColor(CellObj.fgcolor);
    TextBackground(CellObj.bgcolor);
    write(CellObj.symbol);
    GotoXY(1, 1);
    TextAttr := SaveTextAttr;
end;

procedure SetCell(var CellObj: TCell; symbol: char; fgcolor, bgcolor: word;
    IsBlink: boolean);
begin
    CellObj.symbol := symbol;
    CellObj.fgcolor := fgcolor;
    CellObj.bgcolor := bgcolor;
    CellObj.IsBlink := IsBlink;
end;

procedure SetHiddenCell(var CellObj: TCell);
begin
    SetCell(CellObj, HiddenCellSymbol, HiddenCellFgcolor, HiddenCellBgcolor, 
        HiddenCellIsBlink);
end;

procedure SetNumberCell(var CellObj: TCell; number: integer);
begin
    SetCell(CellObj, chr(number + ord('0')), NumberCellFgcolor, 
        NumberCellBgcolor, NumberCellIsBlink)
end;

procedure SetEmptyCell(var CellObj: TCell);
begin
    SetCell(CellObj, EmptyCellSymbol, EmptyCellFgcolor, EmptyCellBgcolor, 
        EmptyCellIsBlink);
end;

procedure SetFlagCell(var CellObj: TCell);
begin
    SetCell(CellObj, FlagCellSymbol, FlagCellFgcolor, FlagCellBgcolor,
        FlagCellIsBlink);
end;

procedure SetMarkedBombCell(var CellObj: TCell);
begin
    SetCell(CellObj, MarkedBombCellSymbol, MarkedBombCellFgcolor, 
        MarkedBombCellBgcolor, MarkedBombCellIsBlink);
end;

procedure SetBombCell(var CellObj: TCell);
begin
    SetCell(CellObj, BombCellSymbol, BombCellFgcolor, BombCellBgcolor,
        BombCellIsBlink);
end;

procedure SetBorderCell(var CellObj: TCell);
begin
    SetCell(CellObj, BorderCellSymbol, BorderCellFgcolor, BorderCellBgcolor,
        BorderCellIsBlink);
end;

procedure SetCursorCell(var CellObj: TCell);
begin
    SetCell(CellObj, CellObj.symbol, CellObj.fgcolor, CursorCellBgcolor,
        CursorCellIsBlink);
end;

end.
