unit cursor;

interface
uses field, cell;
type
    TCursor = record
        x, y: integer;
        UnderCursorCell: CellTypes;
        FieldPtr: TFieldPtr;
    end;

procedure CreateCursor(var CursorObj: TCursor; x, y: integer; 
    var FieldObj: TField);
procedure MoveCursor(var CursorObj: TCursor; dx, dy: integer);
procedure HideCursor(var CursorObj: TCursor);
procedure ShowCursor(var CursorObj: TCursor);
procedure SetUnderCursorCell(var CursorObj: TCursor; CellType: CellTypes);
procedure MarkCursorCell(var CursorObj: TCursor; var FlagsCount: integer);
procedure OpenCursorCell(var CursorObj: TCursor);

implementation

procedure CreateCursor(var CursorObj: TCursor; x, y: integer; 
    var FieldObj: TField);
begin
    CursorObj.x := x;
    CursorObj.y := y;
    CursorObj.FieldPtr := @FieldObj;
    CursorObj.UnderCursorCell := GetFieldCell(FieldObj, x, y);
end;

procedure MoveCursor(var CursorObj: TCursor; dx, dy: integer);
begin
    if (CursorObj.x + dx > 1) and (CursorObj.x + dx < FieldWidth) and
       (CursorObj.y + dy > 1) and (CursorObj.y + dy < FieldHeight)
    then
    begin
        CursorObj.x := CursorObj.x + dx;
        CursorObj.y := CursorObj.y + dy;
        CursorObj.UnderCursorCell := GetFieldCell(CursorObj.FieldPtr^,
            CursorObj.x, CursorObj.y);
    end;
end;

procedure HideCursor(var CursorObj: TCursor);
begin
    SetFieldCell(CursorObj.FieldPtr^, CursorObj.x, CursorObj.y,
        CursorObj.UnderCursorCell); 
end;

procedure ShowCursor(var CursorObj: TCursor);
begin
    SetFieldCell(CursorObj.FieldPtr^, CursorObj.x, CursorObj.y, CCursor); 
end;

procedure SetUnderCursorCell(var CursorObj: TCursor; CellType: CellTypes);
begin
    CursorObj.UnderCursorCell := CellType;
    SetFieldCell(CursorObj.FieldPtr^, CursorObj.x, CursorObj.y, CellType); 
end;

procedure MarkCursorCell(var CursorObj: TCursor; var FlagsCount: integer);
begin
    if CursorObj.UnderCursorCell = CHidden then
    begin
        SetUnderCursorCell(CursorObj, CFlag);
        FlagsCount := FlagsCount + 1;
    end
    else if CursorObj.UnderCursorCell = CFlag then
    begin
        SetUnderCursorCell(CursorObj, CHidden);
        FlagsCount := FlagsCount - 1;
    end;
end;

procedure OpenCursorCell(var CursorObj: TCursor);
begin
    OpenHiddenCell(CursorObj.FieldPtr^, CursorObj.x, CursorObj.y);
    CursorObj.UnderCursorCell := GetFieldCell(CursorObj.FieldPtr^,
        CursorObj.x, CursorObj.y);
end;

end.
