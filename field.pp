unit field;

interface
uses cell;
const
    FieldHeight = 12;
    FieldWidth = 22;
type
    ArrayOfCell = array [1..FieldHeight, 1..FieldWidth] of TCell;
    BombsMap = array [1..FieldHeight, 1..FieldWidth] of boolean;
    TField = record
        x, y: integer;
        cells: ArrayOfCell;
        bombs: BombsMap;
    end;
    TFieldPtr = ^TField;

procedure CreateField(var FieldObj: TField; x, y, BombsCount: integer);
procedure DrawField(var FieldObj: TField);
function GetFieldCell(var FieldObj: TField; x, y: integer): CellTypes;
function CountBombsAroundCell(var FieldObj: TField; x, y: integer): integer;
procedure SetFieldCell(var FieldObj: TField; x, y: integer; 
    CellType: CellTypes);
function IsCellBomb(var FieldObj: TField; x, y: integer): boolean;
function IsBombActive(var FieldObj: TField; x, y: integer): boolean;
procedure ShowFieldBombs(var FieldObj: TField);
procedure OpenHiddenCell(var FieldObj: TField; x, y: integer);

implementation

function GoodBombPosition(var bombs: BombsMap; x, y: integer): boolean;
begin
    if (bombs[y - 1, x] and bombs[y + 1, x] and bombs[y, x - 1] and 
        bombs[y, x + 1]) then
    begin
        GoodBombPosition := false;
        exit;
    end;
    GoodBombPosition := true;
end;

procedure GenerateBombs(var bombs: BombsMap; count: integer);
var
    BombCoordX, BombCoordY: integer;
    i, j: integer;
begin
    for i:=1 to FieldHeight do
        for j:=1 to FieldWidth do
            bombs[i, j] := false;
    for i:=1 to count do
        while true do 
        begin
            BombCoordX := random(FieldWidth - 2) + 1 + 1; { minus 2 borders }
            BombCoordY := random(FieldHeight - 2) + 1 + 1; { minus 2 borders }
            if bombs[BombCoordY, BombCoordX] then
                continue;
            bombs[BombCoordY, BombCoordX] := true;
            if GoodBombPosition(bombs, BombCoordX, BombCoordY) then
                break;
            bombs[BombCoordY, BombCoordX] := false;
        end;
end;

function CountBombsAroundCell(var FieldObj: TField; x, y: integer): integer;
var
    dx, dy: integer;
    count: integer;
begin
    count := 0;
    dx := -1;
    while dx <= 1 do
    begin
        dy := -1;
        while dy <= 1 do
        begin
            if FieldObj.bombs[y + dy, x + dx] then
                count := count + 1; 
            dy := dy + 1;
        end;
        dx := dx + 1;
    end;
    CountBombsAroundCell := count;
end;

procedure SetFieldCell(var FieldObj: TField; x, y: integer; 
    CellType: CellTypes);
begin
    FieldObj.cells[y, x].CellType := CellType;
    case CellType of
        CHidden:
            SetHiddenCell(FieldObj.cells[y, x]);
        CNumber:
            SetNumberCell(FieldObj.cells[y, x], 
                CountBombsAroundCell(FieldObj, x, y));
        CEmpty:
            SetEmptyCell(FieldObj.cells[y, x]);
        CFlag:
            SetFlagCell(FieldObj.cells[y, x]);
        CBomb:
            SetBombCell(FieldObj.cells[y, x]);
        CMarkedBomb:
            SetMarkedBombCell(FieldObj.cells[y, x]);
        CBorder:
            SetBorderCell(FieldObj.cells[y, x]);
        CCursor:
            SetCursorCell(FieldObj.cells[y, x]);
    end;
    DrawCell(FieldObj.cells[y, x], FieldObj.x + x - 1, FieldObj.y + y - 1);
end;

function GetFieldCell(var FieldObj: TField; x, y: integer): CellTypes;
begin
    GetFieldCell := FieldObj.cells[y, x].CellType;
end;

procedure CreateField(var FieldObj: TField; x, y, BombsCount: integer);
var
    i, j: integer;
begin
    randomize;
    FieldObj.x := x;
    FieldObj.y := y;
    GenerateBombs(FieldObj.bombs, BombsCount);
    for i:=1 to FieldWidth do
        SetFieldCell(FieldObj, i, 1, CBorder); { set top board }
    for i:=2 to FieldHeight - 1 do
    begin
        SetFieldCell(FieldObj, 1, i, CBorder); { set left board }
        for j:=2 to FieldWidth - 1 do
            SetFieldCell(FieldObj, j, i, CHidden); 
        SetFieldCell(FieldObj, FieldWidth, i, CBorder); { set right board }
    end;
    { set bottom board }
    for i:=1 to FieldWidth do
        SetFieldCell(FieldObj, i, FieldHeight, CBorder);
end;

procedure DrawField(var FieldObj: TField);
var
    i, j, RealCellX, RealCellY: integer;
begin
    for i:=1 to FieldHeight do
        for j:=1 to FieldWidth do
        begin
            RealCellX := FieldObj.x + j - 1;
            RealCellY := FieldObj.y + i - 1;
            DrawCell(FieldObj.cells[i, j], RealCellX, RealCellY);
        end;
end;

function IsCellBomb(var FieldObj: TField; x, y: integer): boolean;
begin
    IsCellBomb := FieldObj.bombs[y, x];
end;

function IsBombActive(var FieldObj: TField; x, y: integer): boolean;
begin
    IsBombActive := FieldObj.bombs[y, x] and 
        (GetFieldCell(FieldObj, x, y) <> CFlag);
end;

procedure ShowFieldBombs(var FieldObj: TField);
var
    i, j: integer;
    CellType: CellTypes;
begin
    for i:=1 to FieldHeight do
        for j:=1 to FieldWidth do
            if FieldObj.bombs[i, j] then
            begin
                CellType := GetFieldCell(FieldObj, j, i);
                if  CellType = CFlag then
                    SetFieldCell(FieldObj, j, i, CMarkedBomb)
                else if CellType = CCursor then
                begin
                    SetFieldCell(FieldObj, j, i, CBomb);
                    SetFieldCell(FieldObj, j, i, CCursor)
                end
                else
                    SetFieldCell(FieldObj, j, i, CBomb);
            end;
end;

procedure OpenHiddenCell(var FieldObj: TField; x, y: integer);
var
    CellType: CellTypes;
    BombsAround, dx, dy: integer;
begin
    CellType := GetFieldCell(FieldObj, x, y);
    if FieldObj.bombs[y, x] or (CellType <> CHidden) then
        exit;
    BombsAround := CountBombsAroundCell(FieldObj, x, y);
    if BombsAround <> 0 then
    begin
        SetFieldCell(FieldObj, x, y, CNumber);
        exit;
    end;
    SetFieldCell(FieldObj, x, y, CEmpty);
    for dx:=0 to 1 do
        for dy:=0 to 1 do
        begin
            OpenHiddenCell(FieldObj, x + dx, y + dy);
            OpenHiddenCell(FieldObj, x - dx, y + dy);
            OpenHiddenCell(FieldObj, x + dx, y - dy);
            OpenHiddenCell(FieldObj, x - dx, y - dy);
        end;
end;

end.
