unit game;

interface

procedure StartGame(var GameOver: boolean; var win: boolean; 
    var GameTime: TDateTime);

implementation
uses field, keyboard, cell, cursor, display, crt, sysutils;

function CheckWin(var FieldObj: TField): boolean;
var
    i, j: integer;
    CellType: CellTypes;
begin
    for i:=1 to FieldHeight do
        for j:=1 to FieldWidth do
        begin
            CellType := GetFieldCell(FieldObj, j, i);
            if IsBombActive(FieldObj, j, i) or 
                (CellType = CHidden) or
                ((CellType = CFlag) and not IsCellBomb(FieldObj, j, i))
            then
            begin
                CheckWin := false;
                exit;
            end;
        end;
    CheckWin := true;
end;

procedure CreatePauseMenu(var PauseMenu: ListWidget; item1, item2: string);
begin
    CreateListWidget(PauseMenu, 'Pause');
    AddListWidgetItem(PauseMenu, item1);
    AddListWidgetItem(PauseMenu, item2);
end;

procedure StartGame(var GameOver: boolean; var win: boolean; 
    var GameTime: TDateTime);
const
    FrameRate = 60;
    MinBombsCount = 20;
    MaxBombsCount = 30;
    CursorStartX = 2;
    CursorStartY = 2;
    WinMessage = 'You win!';
    LoseMessage = 'You lose!';
    TextFgcolor = LightGray;
    TextBgcolor = Black;
    ExitConfirmMessage = 'Do you want to exit?';
    PauseMenuContinue = 'Continue';
    PauseMenuExit = 'Exit';
var
    FieldObj: TField;
    CursorObj: TCursor;
    key, BombsCount, FlagsCount, FieldX, FieldY, SleepTime: integer;
    FlagsCountTextX, FlagsCountTextY, TimeTextX, TimeTextY: integer;
    ExitConfirm: boolean;
    StartTime, PauseTime: TDateTime;
    PauseMenu: ListWidget;
    PauseOption: string;
begin
    clrscr;
    SleepTime := (1000 + FrameRate - 1) div FrameRate;
    FieldX := (ScreenWidth - FieldWidth + 1) div 2;
    FieldY := (ScreenHeight - FieldHeight + 1) div 2;
    BombsCount := random(MaxBombsCount - MinBombsCount + 1) + MinBombsCount;
    FlagsCount := 0;
    CreateField(FieldObj, FieldX, FieldY, BombsCount);
    CreateCursor(CursorObj, CursorStartX, CursorStartY, FieldObj);
    CreatePauseMenu(PauseMenu, PauseMenuContinue, PauseMenuExit);
    GameOver := false;
    win := false;
    StartTime := time;
    TimeTextX := FieldX;
    TimeTextY := FieldY - 2;
    FlagsCountTextX := FieldX;
    FlagsCountTextY := FieldY - 1;
    DrawField(FieldObj);
    while not GameOver do
    begin
        ShowCursor(CursorObj);
        DrawText('Flags count: ' + 
            IntToStr(BombsCount - FlagsCount), 
            FlagsCountTextX, FlagsCountTextY, TextFgcolor, 
            TextBgColor, false);
        while not KeyPressed do
        begin
            GameTime := Time - StartTime;
            DrawText('Time: ' + TimeToStr(GameTime), 
                TimeTextX, TimeTextY, TextFgcolor, TextBgColor, false);
            delay(SleepTime);
        end;
        GetKey(key);
        HideCursor(CursorObj);
        case key of
            KeyDown, KeyS:
                MoveCursor(CursorObj, 0, 1);
            KeyUp, KeyW:
                MoveCursor(CursorObj, 0, -1);
            KeyLeft, KeyA:
                MoveCursor(CursorObj, -1, 0);
            KeyRight, KeyD:
                MoveCursor(CursorObj, 1, 0);
            KeyF: begin
                MarkCursorCell(CursorObj, FlagsCount);
                ClearLine(FlagsCountTextY);
                win := CheckWin(FieldObj);
                GameOver := win;
            end;
            KeyEnter, KeyC: begin
                GameOver := IsBombActive(CursorObj.FieldPtr^, CursorObj.x,
                    CursorObj.y);
                if not GameOver then
                begin
                    OpenCursorCell(CursorObj);
                    win := CheckWin(FieldObj);
                    GameOver := win;
                end;
            end;
            KeyEsc: begin
                PauseTime := Time;
                ShowListWidget(PauseMenu, PauseOption);
                if PauseOption = PauseMenuExit then
                begin
                    ShowConfirm(ExitConfirmMessage, ExitConfirm, false);
                    if ExitConfirm then
                        break;
                end;
                StartTime := StartTime + (Time - PauseTime);
                clrscr;
                DrawField(FieldObj);
            end;
            KeyQ: begin
                PauseTime := Time;
                ShowConfirm(ExitConfirmMessage, ExitConfirm, false);
                if ExitConfirm then
                    break;
                clrscr;
                DrawField(FieldObj);
                StartTime := StartTime + (Time - PauseTime);
            end;
        end;
    end;
    if GameOver then
    begin
        if not win then
            ShowCursor(CursorObj);
        ShowFieldBombs(FieldObj);
    end;
    RemoveListWidget(PauseMenu);
end;
end.
