unit keyboard;

interface
const
    KeyDown = -80;
    KeyUp = -72;
    KeyLeft = -75;
    KeyRight = -77;
    KeyEsc = 27;
    KeyEnter = 13;
    KeyF = 102;
    KeyQ = 113;
    KeyW = 119;
    KeyS = 115;
    KeyA = 97;
    KeyD = 100;
    KeyC = 99;

procedure GetKey(var n: integer);

implementation
uses crt;

procedure GetKey(var n: integer);
var
    ch: char;
begin
    ch := ReadKey;
    if ch <> #0 then
        n := ord(ch)
    else
    begin
        ch := ReadKey;
        n := -ord(ch);
    end;
end;

end.
