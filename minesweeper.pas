program minesweeper;

uses game, keyboard, display, results, crt, sysutils;

const
    MainMenuTitle = 'Minesweeper';
    MenuOptionStart = 'Start game';
    MenuOptionResults = 'Best results';
    MenuOptionHelp = 'Help';
    MenuOptionAbout = 'Author';
    MenuOptionExit = 'Exit';
    MessageWin = 'You win!';
    MessageLose = 'You lose!';
    MessageHelp = 'W, Up - up\nS, Down - down\n' +
        'A, Left - left\nD, Right - right\nF - set flag\nC, Enter - ' + 
        'open cell\n' + 'Esc - pause\nQ - quit';
    AboutAuthor = 'Hello, world! I am Jakos! :D\n' +
        'This is my terminal minesweeper game\nwritten in Free Pascal.' +
        '\nIt is also an Open Source.\n' +
        'Any bugs? My github: github.com/jakosv';
    WinMessage = 'You win!';
    NewRecordMessage = 'New record!';
    LoseMessage = 'You lose!';
    TextFgcolor = Red;
    TextBgcolor = Black;
var
    SaveTextAttr: integer;
    key: integer;
    MainMenu: ListWidget;
    win, GameOver, RestartGame, NewRecord: boolean;
    MenuOption: string;
    GameTime: TDateTime;
    WinMessageX, WinMessageY, LoseMessageX, LoseMessageY, NewRecordMessageX,
        NewRecordMessageY: integer;
begin
    clrscr;
    SaveTextAttr := TextAttr;
    CreateListWidget(MainMenu, MainMenuTitle);
    AddListWidgetItem(MainMenu, MenuOptionStart);
    AddListWidgetItem(MainMenu, MenuOptionResults);
    AddListWidgetItem(MainMenu, MenuOptionHelp);
    AddListWidgetItem(MainMenu, MenuOptionAbout);
    AddListWidgetItem(MainMenu, MenuOptionExit);
    WinMessageX := (ScreenWidth - length(WinMessage) + 1) div 2;
    WinMessageY := 2;
    LoseMessageX := (ScreenWidth - length(LoseMessage) + 1) div 2;
    LoseMessageY := 2;
    NewRecordMessageX := (ScreenWidth - length(NewRecordMessage) + 1) div 2;
    NewRecordMessageY := 1;
    RestartGame := false;
    while true do
    begin
        clrscr;
        if not RestartGame then
            ShowListWidget(MainMenu, MenuOption);
        if MenuOption = MenuOptionStart then
        begin
            StartGame(GameOver, win, GameTime);
            if GameOver then
            begin
                if win then
                begin
                    NewRecord := false;
                    DrawText(WinMessage, WinMessageX, WinMessageY, TextFgcolor,
                        TextBgcolor, false);
                    AddResult(GameTime, NewRecord);
                    if NewRecord then
                        DrawText(NewRecordMessage, NewRecordMessageX, 
                            NewRecordMessageY, TextFgcolor, TextBgcolor, true);
                end
                else
                    DrawText(LoseMessage, LoseMessageX, LoseMessageY, 
                        TextFgcolor, TextBgcolor, false);
                GetKey(key);
                ShowConfirm('Start new game?', RestartGame, true);
            end
            else
                RestartGame := false;
        end
        else if MenuOption = MenuOptionResults then
        begin
            clrscr;
            ShowResults(MenuOptionResults);
        end
        else if MenuOption = MenuOptionHelp then
            ShowTextBox(MenuOptionHelp, MessageHelp)
        else if MenuOption = MenuOptionAbout then
        begin
            clrscr;
            ShowTextBox(MenuOptionAbout, AboutAuthor)
        end
        else if MenuOption = MenuOptionExit then
            break
        else
            break;
    end;
    RemoveListWidget(MainMenu);
    TextAttr := SaveTextAttr;
    clrscr;
end.
