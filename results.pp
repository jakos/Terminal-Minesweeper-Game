unit results;

interface
uses sysutils;

procedure AddResult(var GameTime: TDateTime; var NewRecord: boolean);
procedure ShowResults(title: string);

implementation
uses display;

const
    filename = '.results';
    MaxResultsCount = 10;

type
    ResultRecord = record
        date, GameTime: TDateTime;
    end;
    FileOfResults = file of ResultRecord;

    ListItemPtr = ^ListItem;
    ListItem = record
        data: ResultRecord;
        next: ListItemPtr;
    end;
    ListOfResults = ^ListItem;


procedure ListAdd(var list: ListOfResults; var NewResult: ResultRecord;
    var ResultPos: integer);
var
    tmp: ListItemPtr;
    PtrToList: ^ListItemPtr;
begin
    ResultPos := 1;
    new(tmp);
    tmp^.data := NewResult;
    tmp^.next := nil;
    if list = nil then
    begin
        list := tmp;
        exit;
    end;
    tmp^.data := NewResult;
    PtrToList := @list;
    while (PtrToList^ <> nil) and 
        (NewResult.GameTime >= PtrToList^^.data.GameTime)
    do
    begin
        PtrToList := @PtrToList^^.next;
        ResultPos := ResultPos + 1;
    end;
    tmp^.next := PtrToList^;
    PtrToList^ := tmp;
end;

procedure ListErase(var list: ListOfResults);
var
    tmp: ListItemPtr;
begin
    while list <> nil do
    begin
        tmp := list;
        list := list^.next;
        dispose(tmp);
    end;
end;

procedure GetListOfResults(var list: ListOfResults; var f: FileOfResults);
var
    tmp: ResultRecord;
    ResultPos: integer;
begin
    {$I-}
    seek(f, 1);
    read(f, tmp);
    list := nil;
    while IOResult = 0 do
    begin
        ListAdd(list, tmp, ResultPos);
        read(f, tmp);
    end;
end;

procedure WriteListOfResults(var list: ListOfResults; var f: FileOfResults);
var
    ResultsCount: integer;
begin
    seek(f, 1);
    ResultsCount := 0;
    while (list <> nil) and (ResultsCount < MaxResultsCount)  do
    begin
        write(f, list^.data);
        ResultsCount := ResultsCount + 1;
        list := list^.next;
    end;
end;

procedure AddResult(var GameTime: TDateTime; var NewRecord: boolean);
var
    f: FileOfResults;
    NewResult: ResultRecord;
    list: ListOfResults;
    ResultPos: integer;
begin
    {$I-}
    assign(f, filename);
    reset(f);
    if IOResult <> 0 then
        rewrite(f);
    NewResult.date := Date;
    NewResult.GameTime := GameTime;
    list := nil;
    GetListOfResults(list, f);
    ListAdd(list, NewResult, ResultPos);
    NewRecord := ResultPos = 1;
    WriteListOfResults(list, f);
    close(f);
    ListErase(list);
end;

procedure ShowResults(title: string);
var
    f: FileOfResults;
    ResultsListWidget: ListWidget;
    list, tmp: ListOfResults;
    ListOption: string;
    ResultsCount: integer;
begin
    {$I-}
    assign(f, filename);
    reset(f);
    if IOResult <> 0 then
        rewrite(f);
    GetListOfResults(list, f);
    close(f);
    ResultsCount := 0;
    CreateListWidget(ResultsListWidget, title); 
    tmp := list;
    while tmp <> nil do
    begin
        AddListWidgetItem(ResultsListWidget, 'Result: ' + 
            TimeToStr(tmp^.data.GameTime) + ' | Date: ' + 
            DateTimeToStr(tmp^.data.date));
        tmp := tmp^.next;
        ResultsCount := ResultsCount + 1;
    end;
    ListErase(list);
    if ResultsCount > 0 then
        ShowListWidget(ResultsListWidget, ListOption)
    else
        ShowTextBox(title, 'No results :(');
    RemoveListWidget(ResultsListWidget);
end;

end.
